import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./views/login/login.component";
import {EmployeeComponent} from "./views/employee/employee.component";
import {QualificationComponent} from "./views/qualification/qualification.component";

const routes: Routes = [
  { path: 'login-view', component: LoginComponent },
  { path: 'employee-view', component: EmployeeComponent },
  { path: 'qualification-view', component: QualificationComponent },
  { path: '', redirectTo: '/login-view', pathMatch: 'full' },
  { path: '**', redirectTo: 'login-view'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
