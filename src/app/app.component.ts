import {Component} from '@angular/core';
import {Employee} from "./Employee";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, of} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  bearer = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIzUFQ0dldiNno5MnlQWk1EWnBqT1U0RjFVN0lwNi1ELUlqQWVGczJPbGU0In0.eyJleHAiOjE2NDUwMjA2NTksImlhdCI6MTY0NTAwNjI1OSwianRpIjoiN2I4MGRkMmEtMDg1ZS00NTZhLTg3NDktOWIwMTc5YzYwY2RkIiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5zenV0LmRldi9hdXRoL3JlYWxtcy9zenV0IiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjU1NDZjZDIxLTk4NTQtNDMyZi1hNDY3LTRkZTNlZWRmNTg4OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVtcGxveWVlLW1hbmFnZW1lbnQtc2VydmljZSIsInNlc3Npb25fc3RhdGUiOiJjZmY4NmRlMy01OGFjLTQ5MzYtODFlZS0yMTM5ZGNlNmI3ZjciLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiZGVmYXVsdC1yb2xlcy1zenV0IiwidW1hX2F1dGhvcml6YXRpb24iLCJ1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ1c2VyIn0.KJjgJBCIn1yGcSLPw9rvzfDbbMKEWfvR59Mx7wDu9TGoRy_ai1oPXZXfv71pJbeI7TXPoTvKMc2rQXYY70I2CkzXKQBLTRMOSZcjGswA5PAGhLSaFVZzQ0REj2A6MI_0c6dbRxcYdvHWp0_xEzTYGwm-0e_Go3MKU3hEiyrCf2dC3BnqRSJUyosDNS5hip0KOg2G6Av9b1AkonDxYLPoYo3B2710LPDPh4zeqkkGldOxYWNKIR-b4qrFncfsbkU1cvhQ-kfIsYUkbYRmPsHjUUEBalFU8KHDyca7V0H8S-3BmlHOm5Cuw4yxz7dmXG5zDAc6Ip-gKDJFnA4Eqwv2Og';
  employees$: Observable<Employee[]>;

  constructor(private http: HttpClient) {
    this.employees$ = of([]);
    this.fetchData();
  }

  fetchData() {
    this.employees$ = this.http.get<Employee[]>('/backend', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    });
  }
}
