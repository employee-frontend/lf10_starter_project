import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginDetailsFormGroup: FormGroup;
  usernameFormControl = new FormControl('', Validators.required);
  passwordFormControl = new FormControl('', Validators.required);

  constructor(private formBuilder: FormBuilder) {
    this.loginDetailsFormGroup = this.formBuilder.group({
      username: this.usernameFormControl,
      password: this.passwordFormControl
    })
  }

  ngOnInit(): void {
  }

}
