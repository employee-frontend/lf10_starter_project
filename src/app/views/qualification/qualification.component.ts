import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Qualification} from "../../Qualification";
import {Observable, of} from "rxjs";
import {Employee} from "../../Employee";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-qualification',
  templateUrl: './qualification.component.html',
  styleUrls: ['./qualification.component.scss']
})
export class QualificationComponent implements OnInit {
  bearer = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIzUFQ0dldiNno5MnlQWk1EWnBqT1U0RjFVN0lwNi1ELUlqQWVGczJPbGU0In0.eyJleHAiOjE2NDUwMTk1MDUsImlhdCI6MTY0NTAwNTEwNSwianRpIjoiZTQyOTQ2YTAtYmViNy00NTk5LTg0YTItMzBkNWIwMDAxZTY4IiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5zenV0LmRldi9hdXRoL3JlYWxtcy9zenV0IiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjU1NDZjZDIxLTk4NTQtNDMyZi1hNDY3LTRkZTNlZWRmNTg4OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVtcGxveWVlLW1hbmFnZW1lbnQtc2VydmljZSIsInNlc3Npb25fc3RhdGUiOiI2Mjg4ZWJkNS0wNDA2LTRkMWEtYWRhMC1lNDY3NmZiMGRjOGUiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiZGVmYXVsdC1yb2xlcy1zenV0IiwidW1hX2F1dGhvcml6YXRpb24iLCJ1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ1c2VyIn0.Rp8zTXoz9vlABQogo3HWESRS72i7TKuzRbKQCQ3tghyg7jacv5sMTfHig7ULouYPnICB_Z5jiCn7zQXLv5slx7dUQPAZnYOBOmkm5ac4QRySZ4-luFsDPQnZ7mcRr2ElJl_yg4udIzE_QoFcf6-ODiMicXca4JjfTZhmTxlQL6dWyJNJnxjtXz1W_BIC8VDS7eLg5JN5LcvNArr1uwgVwOSsToKySgCZyYJjhlAlpQvQLUQew6WT1_YD3Af77NqOnuY4FsV0NoM3axKM_-NaoQU1FLpiYKnb7DjAlPLFEk7WQNBwR5rwVfnk5upQOzwK9494Hem4yghvmZoeZEP7Jg';
  qualifications$: Observable<Employee[]>;

  @Input() entries: Qualification[] = [];
  @Input() displayedColumns: string[] = [];

  qualificationFormGroup: FormGroup;
  designationFormControl = new FormControl('', Validators.required);

  constructor(private formBuilder: FormBuilder, private http: HttpClient) {
    this.qualificationFormGroup = this.formBuilder.group({
      username: this.designationFormControl,
    })

    this.qualifications$ = of([]);
    this.fetchData();
  }

  fetchData() {
    this.qualifications$ = this.http.get<Employee[]>('/backend', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    });
  }

  ngOnInit(): void {
  }

}
